" adding powerline capabilities
set rtp+=/usr/local/lib/python3.7/site-packages/powerline/bindings/vim
set laststatus=2
set t_Co=256
" turn synthax highlight on
:syntax on
" fix backspace key behaviour
set backspace=indent,eol,start

" setting proper 4 space tab/indent behaviour
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab
