# Add custom PS1 prompt
[ ! -t 0 ] && return
if [ $(id -u) -eq 0 ]
then
    export PS1="\[$(tput bold)\]\[$(tput setaf 1)\][\[$(tput setaf 1)\]\u\[$(tput setaf 1)\]@\[$(tput setaf 1)\]\h \[$(tput setaf 1)\]\W\[$(tput setaf 1)\]]\\$ \[$(tput sgr0)\]"
else
    export PS1="\[$(tput bold)\]\[$(tput setaf 4)\][\[$(tput setaf 6)\]\u\[$(tput setaf 4)\]@\[$(tput setaf 4)\]\h \[$(tput setaf 2)\]\W\[$(tput setaf 4)\]]\\$ \[$(tput sgr0)\]"
fi

# Add auto completion to aws
complete -C aws_completer aws

# Colorize ls' output
alias ls='ls -G'

# Adding powerline
source /usr/local/lib/python3.7/site-packages/powerline/bindings/bash/powerline.sh

